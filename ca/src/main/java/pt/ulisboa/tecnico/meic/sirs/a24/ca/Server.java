package pt.ulisboa.tecnico.meic.sirs.a24.ca;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.security.KeyStore;
import java.util.Properties;
import java.util.concurrent.Executors;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLEngine;
import javax.net.ssl.SSLParameters;
import javax.net.ssl.TrustManagerFactory;

import com.google.common.io.Resources;
import com.sun.net.httpserver.HttpsConfigurator;
import com.sun.net.httpserver.HttpsParameters;
import com.sun.net.httpserver.HttpsServer;

import pt.ulisboa.tecnico.meic.sirs.a24.ca.handler.CertificateHandler;

public class Server {

  private static int port = 443;
  private static String keyStorePassword = "password";
  private static String keyStoreName = "ca.jks";

  public static void main(String[] args) throws Exception {
    var props = loadProperties("app.properties");
    if (props == null) {
      return;
    }

    props.forEach((key, value) -> System.setProperty((String) key, (String) value));

    port = Integer.getInteger("securesms.ca.port", port);
    keyStorePassword = System.getProperty("securesms.ca.keystore.password", keyStorePassword);
    keyStoreName = System.getProperty("securesms.ca.keystore.name", keyStoreName);

    KeyStore keyStore = KeyStore.getInstance("JKS");
    InputStream is = Resources.getResource(keyStoreName).openStream();
    keyStore.load(is, keyStorePassword.toCharArray());

    KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
    kmf.init(keyStore, keyStorePassword.toCharArray());

    TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
    tmf.init(keyStore);


    SSLContext sslContext = SSLContext.getInstance("TLSv1.2");
    sslContext.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);

    HttpsServer server = HttpsServer.create(new InetSocketAddress(port), 0);
    server.setHttpsConfigurator(new HttpsConfigurator(sslContext) {
      public void configure(HttpsParameters params) {
        try {
          SSLContext c = SSLContext.getDefault();
          SSLEngine engine = c.createSSLEngine();
          params.setNeedClientAuth(false);
          params.setCipherSuites (engine.getEnabledCipherSuites());
          params.setProtocols (engine.getEnabledProtocols());

          SSLParameters defaultSSLParameters = c.getDefaultSSLParameters();
          params.setSSLParameters(defaultSSLParameters);
        }
        catch (Exception ignored) {
          //kek pokemon
        }
      }
    });

    server.setExecutor(Executors.newCachedThreadPool());

    server.createContext("/", new CertificateHandler());
    server.start();
    System.out.println("CA server is running");


  }

  private static Properties loadProperties(String name) {
    var props = new Properties();

    try (var stream = Resources.getResource(name).openStream()) {
      props.load(stream);
    } catch (IOException ioe) {
      System.err.printf("Failed loading '%s': %s%n", name, ioe.getMessage());
      return null;
    }

    return props;
  }
}
