package pt.ulisboa.tecnico.meic.sirs.a24.ca.handler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.util.Locale;
import java.util.stream.Collectors;

import com.google.common.io.Resources;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

public class CertificateHandler implements HttpHandler {

  @Override
  public void handle(HttpExchange exchange) throws IOException {
    String response = "Muffin time!";
    int code = HttpURLConnection.HTTP_BAD_METHOD;

    if (exchange.getRequestMethod().equals("GET")) {
      String uri = exchange.getRequestURI().getQuery();

      if (uri != null) {
        String[] tokens = uri.split("=");
        if (tokens.length != 2 || !tokens[0].equals("entity")) {
          code = HttpURLConnection.HTTP_BAD_REQUEST;
        } else {
          if ("bank".equals(tokens[1])) {
            String fileName = tokens[1] + ".pem";
            System.out.println("Opening file " + fileName + "...");
            InputStream is = Resources.getResource(fileName).openStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            response = br.lines().collect(Collectors.joining("\n"));
            code = HttpURLConnection.HTTP_OK;
          }
        }
      } else {
        code = HttpURLConnection.HTTP_BAD_REQUEST;
      }
    }

    System.out.println(String.format(Locale.getDefault(), "Sending response with code %d", code));
    exchange.sendResponseHeaders(code, response.length());
    exchange.getResponseBody().write(response.getBytes());
    exchange.close();
  }
}
