package pt.ulisboa.tecnico.meic.sirs.a24.bank.runnable;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TcpService implements Runnable {

  private final ServerSocket serverSocket;
  private final ExecutorService pool;

  public TcpService(int port) throws IOException {
    serverSocket = new ServerSocket(port);
    pool = Executors.newCachedThreadPool();
  }

  public void run() { // run the service
    System.out.println("Listening for TCP connections on port " + serverSocket.getLocalPort());

    while (!Thread.currentThread().isInterrupted()) {
      try {
        Socket clientSocket = serverSocket.accept();
        pool.execute(new TcpHandler(clientSocket));
      } catch (SocketTimeoutException ignored) {
      } catch (IOException ioe) {
        ioe.printStackTrace();
        break;
      }
    }

    pool.shutdown();
  }
}

