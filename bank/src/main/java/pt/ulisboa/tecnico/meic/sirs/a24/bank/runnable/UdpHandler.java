package pt.ulisboa.tecnico.meic.sirs.a24.bank.runnable;

import java.net.DatagramPacket;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.KeyStore;
import java.security.KeyStore.PasswordProtection;
import java.security.KeyStore.SecretKeyEntry;
import java.security.PublicKey;
import java.security.spec.KeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;
import java.util.stream.Stream;
import javax.crypto.SecretKey;

import com.google.common.io.BaseEncoding;

import org.hibernate.Session;

import pt.ulisboa.tecnico.meic.sirs.a24.bank.Server;
import pt.ulisboa.tecnico.meic.sirs.a24.bank.model.User;
import pt.ulisboa.tecnico.meic.sirs.a24.security.CryptoUtil;

class UdpHandler implements Runnable {

  private static final BaseEncoding base64 = BaseEncoding.base64();

  private DatagramPacket packet;

  UdpHandler(DatagramPacket packet) {
    this.packet = packet;
  }

  public void run() {
    Charset charset = StandardCharsets.UTF_8;
    ByteBuffer buffer = ByteBuffer.wrap(packet.getData(), 0, packet.getLength());

    byte nameLength = buffer.get();
    byte cipherTextSize = buffer.get();
    byte signatureSize = buffer.get();

    byte[] usernameBytes = new byte[nameLength]; buffer.get(usernameBytes);
    String username = new String(usernameBytes, charset);

    byte[] iv = new byte[16]; buffer.get(iv);

    byte[] cipherText = new byte[cipherTextSize]; buffer.get(cipherText);
    byte[] signature = new byte[signatureSize]; buffer.get(signature);

    try (Session session = Server.sessionFactory().openSession()) {
      session.beginTransaction();
      User user = session.createQuery("from User where username = :name", User.class)
          .setParameter("name", username)
          .uniqueResult();

      if (user == null) {
        System.out.println(String.format("User %s does not exist on the system ", username));
        session.getTransaction().rollback();
        return;
      }

      if (!user.addNonce(iv)) {
        //TODO: handle error
      } else {
        KeyStore ks = Server.getKeyStore();

        SecretKey key = ((SecretKeyEntry) ks.getEntry(user.getUsername(),
            new PasswordProtection(Server.getKeyStorePassword().toCharArray()))).getSecretKey();

        String[] decrypted = new String(CryptoUtil.decrypt(key.getEncoded(), iv, cipherText))
            .split(" ");
        String iban = decrypted[0];
        String amount = decrypted[1];

        byte[][] trans = Stream.of(iban, amount)
            .map(s -> s.getBytes(charset))
            .toArray(byte[][]::new);
        byte[] toVerify = ByteBuffer.allocate(trans[0].length + trans[1].length + iv.length)
            .put(trans[0])
            .put(trans[1])
            .put(iv).array();

        KeyFactory kf = KeyFactory.getInstance("EC");
        KeySpec spec = new X509EncodedKeySpec(user.getPublicKey());
        PublicKey clientKey = kf.generatePublic(spec);

        if (CryptoUtil.verifySignature(clientKey, toVerify, signature, "SHA256withECDSA")) {
          System.out
              .printf("User %s transferred %s to IBAN %s%n", user.getUsername(), amount, iban);
          //TODO: handle input
        } else {
          System.out.println(
              String.format("Message from user %s had an invalid signature", user.getUsername()));
          //TODO: handle invalid signature
          session.getTransaction().rollback();
          return;
        }
        session.getTransaction().commit();
      }
    } catch (GeneralSecurityException gse) {
      gse.printStackTrace();
    }
  }
}
