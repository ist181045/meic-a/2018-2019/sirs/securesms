package pt.ulisboa.tecnico.meic.sirs.a24.bank.runnable;

import java.io.IOException;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.KeyStore.PasswordProtection;
import java.security.KeyStore.SecretKeyEntry;
import java.security.KeyStoreException;
import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.persistence.NoResultException;

import com.google.common.hash.Hashing;
import com.google.common.io.BaseEncoding;

import org.hibernate.Session;
import org.hibernate.Transaction;

import pt.ulisboa.tecnico.meic.sirs.a24.Channel;
import pt.ulisboa.tecnico.meic.sirs.a24.Message;
import pt.ulisboa.tecnico.meic.sirs.a24.bank.Server;
import pt.ulisboa.tecnico.meic.sirs.a24.bank.model.User;
import pt.ulisboa.tecnico.meic.sirs.a24.security.CryptoUtil;
import pt.ulisboa.tecnico.meic.sirs.a24.security.EllipticCurveDiffieHellman;

class TcpHandler implements Runnable {

  private final Channel channel;
  private byte[] sessionKey;

  TcpHandler(Socket socket) {
    channel = new Channel(socket);
  }

  public void run() {
    var base64 = BaseEncoding.base64();
    var ecdh = new EllipticCurveDiffieHellman();

    try (channel) {
      var localAddr = channel.socket().getLocalSocketAddress();
      var remoteAddr = channel.socket().getRemoteSocketAddress();

      while (!Thread.currentThread().isInterrupted() && !channel.isClosed()) {
        Message inbound = channel.read();
        var outbound = new Message(inbound.opCode(), "it's muffin time");

        System.out.println(remoteAddr + " ---> " + localAddr + " ::: " + inbound);

        switch (inbound.opCode()) {
          case Message.KEY_EXCHANGE:
            byte[] ownPk = ecdh.publicKey().getEncoded();
            byte[] otherPk = base64.decode(inbound.body());

            var privKey = Server.getPrivateKey();
            var algo = "SHA256with" + privKey.getAlgorithm();
            byte[] sig = CryptoUtil.sign(privKey, ownPk, algo);
            privKey = null; // let it go

            sessionKey = ecdh.generateSessionKey(otherPk);
            //System.out.println("Key: " + base64.encode(sessionKey));

            outbound.body(Stream.of(sig, ownPk)
                .map(base64::encode)
                .collect(Collectors.joining(":")));
            break;
          case Message.AUTHENTICATE:
            // 0 = hmac, 1 = iv, 2 = pubKey, 3 = ciphertext
            var decoded = Arrays.stream(inbound.body().split(":"))
                .map(base64::decode)
                .toArray(byte[][]::new);

            byte[] iv = decoded[1];
            byte[] pk = decoded[2];

            String[] plain = new String(CryptoUtil.decrypt(sessionKey, iv, decoded[3])).split(":");
            boolean integral = Arrays.equals(Hashing.hmacSha256(sessionKey).newHasher()
                .putBytes(iv)
                .putBytes(pk)
                .putString(plain[0], StandardCharsets.UTF_8) // username
                .putString(plain[1], StandardCharsets.UTF_8) // pin code
                .hash().asBytes(), decoded[0]);

            if (!integral) {
              //TODO: decide what to do here
            }

            Transaction tx = null;

            try (Session session = Server.sessionFactory().openSession()) {
              tx = session.beginTransaction();

              User user = session.createQuery("from User where username = :name", User.class)
                  .setParameter("name", plain[0])
                  .getSingleResult();

              boolean verified = user.verifyPassword(plain[1]);

              if (!verified) {
                throw new IllegalArgumentException("Wrong password");
              }

              session.createQuery("update User set publicKey = :pk where username = :name")
                  .setParameter("pk", pk)
                  .setParameter("name", plain[0])
                  .executeUpdate();

              SecretKey key = new SecretKeySpec(sessionKey, "AES");
              SecretKeyEntry entry = new SecretKeyEntry(key);
              Server.getKeyStore().setEntry(plain[0], entry,
                  new PasswordProtection(Server.getKeyStorePassword().toCharArray()));
              tx.commit();

              System.out.println(
                  String.format("User %s is ready to communicate through SMS", plain[0]));
              outbound.body(String.valueOf(verified));
            } catch (NoResultException | IllegalArgumentException e) {
              if (tx != null) {
                tx.rollback();
              }

              outbound.opCode(Message.USER_NOT_FOUND);
              outbound.body("Failed to login: " + e.getMessage());
              Thread.currentThread().interrupt();
            } catch (KeyStoreException e) {
              e.printStackTrace();
            }

            break;
          case Message.DONE:
            outbound.body("Goodbye");
            Thread.currentThread().interrupt();
            break;
          default:
            outbound.opCode(Message.MUFFIN_TIME); // MUFFIN TIME
            Thread.currentThread().interrupt();
            break;
        }

        System.out.println(localAddr + " ---> " + remoteAddr + " ::: " + outbound);

        channel.write(outbound);
      }
    } catch (IOException | InvalidKeyException ignored) {
    }
  }
}
