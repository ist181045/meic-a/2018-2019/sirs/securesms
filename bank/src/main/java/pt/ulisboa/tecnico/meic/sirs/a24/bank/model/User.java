package pt.ulisboa.tecnico.meic.sirs.a24.bank.model;

import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import com.google.common.hash.Hashing;
import com.google.common.io.BaseEncoding;

import org.hibernate.annotations.NaturalId;

@Entity
public class User {

  @Id
  @GeneratedValue
  private UUID id;

  @Column(length = 32)
  @NaturalId(mutable = true)
  private String username;

  private String password;
  private byte[] publicKey;

  @ElementCollection
  private Set<String> nonces = new HashSet<>();

  public User() {
  }

  public User(String username, String password) {
    this.username = username;
    setPassword(password);
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public Set<String> getNonces() {
    return Collections.unmodifiableSet(nonces);
  }

  public boolean addNonce(String nonce) {
    return nonces.add(nonce);
  }

  public boolean addNonce(byte[] nonce) {
    return addNonce(new String(nonce));
  }

  public boolean removeNonce(String nonce) {
    return nonces.remove(nonce);
  }

  public boolean removeNonce(byte[] nonce) {
    return removeNonce(new String(nonce));
  }

  public void setPassword(String password) {
    byte[] salt = secureBytes(32);
    this.password = Stream.of(salt, hashPassword(password, salt))
        .map(BaseEncoding.base64()::encode)
        .collect(Collectors.joining(":"));
  }

  public boolean verifyPassword(String password) {
    var data = Arrays.stream(this.password.split(":"))
        .map(BaseEncoding.base64()::decode)
        .toArray(byte[][]::new);

    return Arrays.equals(data[1], hashPassword(password, data[0]));
  }

  public byte[] getPublicKey() {
    return publicKey;
  }

  public void setPublicKey(byte[] publicKey) {
    this.publicKey = publicKey;
  }

  private byte[] secureBytes(int len) {
    var bytes = new byte[len];
    new SecureRandom().nextBytes(bytes);
    return bytes;
  }

  private byte[] hashPassword(String password, byte[] salt) {
    return Hashing.hmacSha512(password.getBytes(StandardCharsets.UTF_8))
        .hashBytes(salt)
        .asBytes();
  }
}
