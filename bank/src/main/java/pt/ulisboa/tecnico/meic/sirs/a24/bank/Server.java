package pt.ulisboa.tecnico.meic.sirs.a24.bank;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Properties;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import com.google.common.io.BaseEncoding;
import com.google.common.io.Resources;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import pt.ulisboa.tecnico.meic.sirs.a24.bank.model.User;
import pt.ulisboa.tecnico.meic.sirs.a24.bank.runnable.TcpService;
import pt.ulisboa.tecnico.meic.sirs.a24.bank.runnable.UdpService;
import pt.ulisboa.tecnico.meic.sirs.a24.hibernate.SnakeCasePhysicalNamingStrategy;

public class Server {

  private static SessionFactory sessionFactory;
  private static PrivateKey serverKey;
  private static KeyStore ks;

  private static String host = "localhost";
  private static int tcpPort = 4000;
  private static int udpPort = 4001;

  public static void main(String[] args) {
    var props = loadProperties("app.properties");
    if (props == null) {
      return;
    }

    props.forEach((key, value) -> System.setProperty((String) key, (String) value));

    host = System.getProperty("securessms.bank.host", host);
    tcpPort = Integer.getInteger("securesms.bank.tcp.port", tcpPort);
    udpPort = Integer.getInteger("securesms.bank.udp.port", udpPort);

    assert tcpPort != udpPort;

    getServerPrivateKey();
    loadKeyStore();
    bootstrapHibernate();
    populate();
    startTcpServer();
    startUdpServer();

    System.out.println("Press <Enter> to stop..");
    try (var in = new Scanner(System.in)) {
      in.nextLine();
    }

    System.exit(0);
  }

  public static SessionFactory sessionFactory() {
    return sessionFactory;
  }

  public static PrivateKey getPrivateKey() {
    return serverKey;
  }

  public static String getKeyStorePassword() {
    return System.getProperty("securesms.keystore.password", "s3cr3t");
  }

  private static Properties loadProperties(String name) {
    var props = new Properties();

    try (var stream = Resources.getResource(name).openStream()) {
      props.load(stream);
    } catch (IOException ioe) {
      System.err.printf("Failed loading '%s': %s%n", name, ioe.getMessage());
      return null;
    }

    return props;
  }

  private static void bootstrapHibernate() {
    Logger.getLogger("org.hibernate").setLevel(Level.SEVERE); // Please shut up

    var cfg = new Configuration().configure();
    cfg.setPhysicalNamingStrategy(new SnakeCasePhysicalNamingStrategy());
    sessionFactory = cfg.buildSessionFactory();

    Runtime.getRuntime().addShutdownHook(new Thread(sessionFactory::close));
  }

  private static void populate() {
    try (var session = sessionFactory().openSession()) {
      session.beginTransaction();

      session.save(new User("Amy", "100101"));
      session.save(new User("Jimmy", "777333"));
      session.save(new User("Timmy", "654321"));

      session.getTransaction().commit();
    }
  }

  private static void getServerPrivateKey() {
    try (InputStream is = Resources.getResource("key.pem").openStream()) {
      String key = new BufferedReader(new InputStreamReader(is)).lines()
          .filter(l -> !l.startsWith("-"))
          .collect(Collectors.joining());

      var spec = new PKCS8EncodedKeySpec(BaseEncoding.base64().decode(key));
      serverKey = KeyFactory.getInstance("RSA").generatePrivate(spec);
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (NoSuchAlgorithmException ignored) {
    } catch (InvalidKeySpecException e) {
      e.printStackTrace();
    }
  }

  private static void startTcpServer() {
    Thread thread;

    try {
      thread = new Thread(new TcpService(tcpPort));
      thread.start();
      Runtime.getRuntime().addShutdownHook(new Thread(() -> interruptAndJoin(thread, 2000)));
    } catch (IOException e) {
      e.printStackTrace();
      System.exit(1);
    }
  }

  private static void startUdpServer() {
    Thread thread;

    try {
      thread = new Thread(new UdpService(udpPort));
      thread.start();
      Runtime.getRuntime().addShutdownHook(new Thread(() -> interruptAndJoin(thread, 2000)));
    } catch (IOException e) {
      e.printStackTrace();
      System.exit(1);
    }
  }

  private static void interruptAndJoin(Thread thread, int limit) {
    thread.interrupt();
    try {
      thread.join(limit);
    } catch (InterruptedException ignored) {
      System.err.println("Join timed out, ignoring");
    }
  }

  private static void loadKeyStore() {
    try {
      ks = KeyStore.getInstance(KeyStore.getDefaultType());
      ks.load(null);
    } catch (GeneralSecurityException | IOException e) {
      throw new RuntimeException(e);
    }
  }

  public static KeyStore getKeyStore() {
    return ks;
  }
}
