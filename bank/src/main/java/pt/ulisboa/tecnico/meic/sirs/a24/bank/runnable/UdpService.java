package pt.ulisboa.tecnico.meic.sirs.a24.bank.runnable;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketTimeoutException;
import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class UdpService implements Runnable {

  private final DatagramSocket serverSocket;
  private final ExecutorService pool;

  public UdpService(int port) throws IOException {
    serverSocket = new DatagramSocket(port);
    pool = Executors.newCachedThreadPool();
  }

  public void run() { // run the service
    System.out.println("Listening for UDP packets on port " + serverSocket.getLocalPort());

    while (!Thread.currentThread().isInterrupted()) {
      byte[] buffer = new byte[250];
      Arrays.fill(buffer, (byte) 0);

      try {
        DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
        serverSocket.receive(packet);
        pool.execute(new UdpHandler(packet));
      } catch (IOException ioe) {
        ioe.printStackTrace();
      }
    }

    pool.shutdown();
  }
}

