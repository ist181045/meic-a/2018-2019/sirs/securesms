package pt.ulisboa.tecnico.meic.sirs.a24.android.app.service

import android.content.Context
import okhttp3.OkHttpClient
import pt.ulisboa.tecnico.meic.sirs.a24.android.app.SecureSmsApplication
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query
import java.security.KeyStore
import java.security.cert.CertificateFactory
import java.security.cert.X509Certificate
import javax.net.ssl.SSLContext
import javax.net.ssl.SSLSocketFactory
import javax.net.ssl.TrustManager
import javax.net.ssl.TrustManagerFactory
import javax.net.ssl.X509TrustManager

interface CaService {
    @GET("/")
    fun getCertificate(@Query("entity") entity: String): Call<String>

    companion object {
        private var client: Retrofit? = null

        fun client(context: Context): Retrofit {
            client = client ?: Retrofit.Builder()
                .baseUrl("https://${SecureSmsApplication.caHost}:${SecureSmsApplication.caPort}")
                .addConverterFactory(ScalarsConverterFactory.create())
                .client(object : OkHttpClient() {
                    override fun sslSocketFactory(): SSLSocketFactory {
                        val cert = context.assets.open("ca.pem").use {
                            CertificateFactory.getInstance("X.509").generateCertificate(it)
                        }
                        val tmf =
                            TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm())
                                .apply {
                                    init(KeyStore.getInstance(KeyStore.getDefaultType()).apply {
                                        load(null, null)
                                        setCertificateEntry("a24", cert)
                                    })
                                }

                        return SSLContext.getInstance("TLS").apply {
                            init(null, arrayOf<TrustManager>(object : X509TrustManager {
                                override fun checkClientTrusted(
                                    chain: Array<out X509Certificate>?,
                                    authType: String?
                                ) {
                                }

                                override fun checkServerTrusted(
                                    chain: Array<out X509Certificate>?,
                                    authType: String?
                                ) {
                                }

                                override fun getAcceptedIssuers() = arrayOf<X509Certificate>()
                            }), null)
                        }.socketFactory
                    }
                }).build()

            return client!!
        }
    }
}
