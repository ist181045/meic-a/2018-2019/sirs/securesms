package pt.ulisboa.tecnico.meic.sirs.a24.android.app

import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_login.loginButton
import kotlinx.android.synthetic.main.activity_login.pinCode
import kotlinx.android.synthetic.main.activity_login.username
import pt.ulisboa.tecnico.meic.sirs.a24.android.app.runnable.KeyExchangeTask

class LoginActivity : AppCompatActivity() {

    private var exchangeTask = KeyExchangeTask(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        with(loginButton) {
            setOnClickListener {
                Log.v(TAG, "Clicked login button")
                if (username.string.isEmpty() || pinCode.string.isEmpty()) {
                    Toast.makeText(
                        this@LoginActivity,
                        "Please fill login fields",
                        Toast.LENGTH_SHORT
                    ).show()
                } else if (visibility == View.VISIBLE) {
                    val imm = (getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
                    imm.hideSoftInputFromWindow(currentFocus?.windowToken, 0)

                    Log.v(TAG, "Executing key exchange task")
                    with(exchangeTask) {
                        when (status) {
                            AsyncTask.Status.FINISHED -> KeyExchangeTask(this@LoginActivity)
                            else -> this
                        }
                    }.execute(User(username.string.trim(), pinCode.string.trim()))
                }
            }
        }

        with(pinCode) {
            setOnEditorActionListener { _, _, _ ->
                Log.v(TAG, "Received DONE action")
                loginButton.performClick()
            }

            setOnKeyListener { _, _, event ->
                Log.v(TAG, "Received key: ${KeyEvent.keyCodeToString(event.keyCode)}")
                val maxLength = resources.getInteger(R.integer.max_pin_code_length)
                when (event?.keyCode) {
                    KeyEvent.KEYCODE_DEL, KeyEvent.KEYCODE_FORWARD_DEL -> false
                    else -> length() == maxLength && loginButton.performClick()
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            LOGIN_REQUEST -> when (resultCode) {
                MainActivity.RESULT_NO_USER -> Toast.makeText(
                    this,
                    "Problem transferring user",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    companion object {
        private const val TAG = "LoginActivity"
        public const val LOGIN_REQUEST = 142
    }
}
