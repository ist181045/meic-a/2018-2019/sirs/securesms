package pt.ulisboa.tecnico.meic.sirs.a24.android.app

import android.app.Activity
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.messageBox
import kotlinx.android.synthetic.main.activity_main.messageView
import kotlinx.android.synthetic.main.activity_main.sendButton
import pt.ulisboa.tecnico.meic.sirs.a24.android.app.R.id.messageBox
import pt.ulisboa.tecnico.meic.sirs.a24.android.app.R.id.messageView
import pt.ulisboa.tecnico.meic.sirs.a24.android.app.R.id.sendButton
import pt.ulisboa.tecnico.meic.sirs.a24.android.app.runnable.SendSmsTask

class MainActivity : AppCompatActivity() {

    private lateinit var adapter: MessageAdapter
    private val user by lazy {
        intent?.getParcelableExtra<User>(USER_EXTRA)
    }

    override fun onStart() {
        super.onStart()
        if (!intent?.hasExtra(USER_EXTRA)!!) {
            setResult(RESULT_NO_USER)
            finish()
        }
        setResult(Activity.RESULT_OK)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        messageView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, true)
        adapter = MessageAdapter(ArrayList()).apply {
            with(messages) {
                add("PT50AAAABBBBCCCCCCCCCCKK 100.50")
                add("FR41AAAABBBBCCCCCCCCCCKK 50.65")
                add("DE12AAAABBBBCCCCCCCCCCKK 0.46")
            }

            messageView.adapter = this
        }

        with(messageBox) {
            setOnEditorActionListener { _, _, _ -> onKeyAction() }
            setOnKeyListener { _, _, _ -> onKeyAction() }
        }

        with(sendButton) {
            setOnClickListener {
                isClickable = false
                SendSmsTask(user).execute(parseSms(messageBox.string))
                adapter.messages.add(0, messageBox.string)
                adapter.notifyDataSetChanged()
                messageBox.setText("")
            }

            isClickable = false
        }
    }

    private fun parseSms(text: String) = with(text.split(Regex(" +"))) {
        val (iban, amount) = this
        Transfer(iban.apply {
            replace(Regex(" +"), "")
        }, amount.run {
            (toFloat() * 100).toInt().toString()
        })
    }

    private fun onKeyAction(): Boolean {
        sendButton.isClickable = messageBox.text.trim().isNotEmpty()
        return false
    }

    companion object {
        private const val TAG = "MainActivity"
        public const val USER_EXTRA = "USER_EXTRA"
        public const val RESULT_NO_USER = 523
    }
}
