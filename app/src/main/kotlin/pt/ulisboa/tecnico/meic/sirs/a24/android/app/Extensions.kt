package pt.ulisboa.tecnico.meic.sirs.a24.android.app

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.annotation.LayoutRes
import javax.net.ssl.TrustManagerFactory

public val EditText.string get() = this.text.toString()
public fun ViewGroup.inflate(@LayoutRes res: Int, attachToRoot: Boolean = false): View =
    LayoutInflater.from(context).inflate(res, this, attachToRoot)
