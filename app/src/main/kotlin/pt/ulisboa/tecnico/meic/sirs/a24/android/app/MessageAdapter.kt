package pt.ulisboa.tecnico.meic.sirs.a24.android.app

import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class MessageAdapter(val messages: MutableList<String>) :
    RecyclerView.Adapter<MessageAdapter.Holder>() {

    override fun onCreateViewHolder(parent: ViewGroup, type: Int) =
        Holder(parent.inflate(R.layout.message_list_item))

    override fun getItemCount() = messages.size

    override fun onBindViewHolder(holder: Holder, position: Int) = holder.bind(messages[position])

    class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(message: String) {
            (itemView as TextView).text = message
        }
    }
}