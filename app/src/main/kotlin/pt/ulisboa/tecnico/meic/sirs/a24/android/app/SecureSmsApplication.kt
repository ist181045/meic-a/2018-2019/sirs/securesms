package pt.ulisboa.tecnico.meic.sirs.a24.android.app

import android.app.Application
import android.util.Log
import java.io.IOException
import java.util.Properties

@Suppress("unused")
class SecureSmsApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        assets.open(APP_PROPERTIES).use {
            try {
                Properties().apply { load(it) }.forEach { k, v ->
                    Log.v(TAG, "Setting system property: $k => $v")
                    System.setProperty(k as String, v as String)
                }
            } catch (ioe: IOException) {
                Log.e(TAG, "Failed to load properties file '$APP_PROPERTIES'", ioe)
                Log.v(TAG, Log.getStackTraceString(ioe))
                throw RuntimeException(ioe)
            }
        }
    }

    companion object {
        private const val TAG = "SecureSmsApplication"
        private const val APP_PROPERTIES = "app.properties"

        val bankHost by lazy { System.getProperty("securesms.bank.host", "10.0.2.2")!! }
        val bankTcpPort by lazy { Integer.getInteger("securesms.bank.tcp.port", 4000)!! }
        val bankUdpPort by lazy { Integer.getInteger("securesms.bank.udp.port", 4001)!! }

        val caHost by lazy { System.getProperty("securesms.ca.host", "10.0.2.2")!! }
        val caPort by lazy { Integer.getInteger("securesms.ca.port", 44343)!! }
    }
}
