package pt.ulisboa.tecnico.meic.sirs.a24.android.app

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class User(val username: String, val password: String) : Parcelable

data class Transfer(val iban: String, val amount: String) {
    override fun toString() = "$iban $amount"
}
