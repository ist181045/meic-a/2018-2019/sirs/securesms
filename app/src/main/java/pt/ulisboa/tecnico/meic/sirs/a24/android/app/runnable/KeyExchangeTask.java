package pt.ulisboa.tecnico.meic.sirs.a24.android.app.runnable;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.Socket;
import java.net.SocketAddress;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStore.SecretKeyEntry;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.security.keystore.KeyProtection;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.common.hash.Hashing;
import com.google.common.io.BaseEncoding;

import pt.ulisboa.tecnico.meic.sirs.a24.Channel;
import pt.ulisboa.tecnico.meic.sirs.a24.Message;
import pt.ulisboa.tecnico.meic.sirs.a24.android.app.LoginActivity;
import pt.ulisboa.tecnico.meic.sirs.a24.android.app.MainActivity;
import pt.ulisboa.tecnico.meic.sirs.a24.android.app.R;
import pt.ulisboa.tecnico.meic.sirs.a24.android.app.SecureSmsApplication;
import pt.ulisboa.tecnico.meic.sirs.a24.android.app.User;
import pt.ulisboa.tecnico.meic.sirs.a24.android.app.service.CaService;
import pt.ulisboa.tecnico.meic.sirs.a24.security.CryptoUtil;
import pt.ulisboa.tecnico.meic.sirs.a24.security.EllipticCurveDiffieHellman;

import retrofit2.Response;

public class KeyExchangeTask extends AsyncTask<User, String, Boolean> {

  private static final String TAG = "KeyExchangeTask";

  private final WeakReference<Activity> activityRef;
  private final int bankTcpPort = SecureSmsApplication.Companion.getBankTcpPort();
  private final String bankHost = SecureSmsApplication.Companion.getBankHost();

  private User user;
  private X509Certificate bankCert;
  private byte[] sessionKey;

  public KeyExchangeTask(Activity activity) {
    activityRef = new WeakReference<>(activity);
  }

  @Override
  protected void onPreExecute() {
    Activity activity = activityRef.get();
    if (activity == null || activity.isFinishing()) {
      Log.d(TAG, "Activity finishing or null before task");
      return;
    }

    activity.findViewById(R.id.loginButton).setVisibility(View.INVISIBLE);
    activity.findViewById(R.id.progressBar).setVisibility(View.VISIBLE);
  }

  @Override
  protected void onPostExecute(Boolean verified) {
    Activity activity = activityRef.get();
    if (activity == null || activity.isFinishing()) {
      Log.d(TAG, "Activity finishing or null after task");
      return;
    }

    if (verified) {
      Intent intent = new Intent(activity.getApplicationContext(), MainActivity.class)
          .putExtra(MainActivity.USER_EXTRA, user);
      activity.startActivityForResult(intent, LoginActivity.LOGIN_REQUEST);
    }
    activity.findViewById(R.id.progressBar).setVisibility(View.GONE);
    activity.findViewById(R.id.loginButton).setVisibility(View.VISIBLE);
  }

  @Override
  protected void onProgressUpdate(String... values) {
    Activity activity = activityRef.get();
    if (activity == null || activity.isFinishing()) {
      return;
    }

    Log.d(TAG, values[0]);
    Toast.makeText(activity.getApplicationContext(), values[0], Toast.LENGTH_SHORT).show();
  }

  @Override
  protected Boolean doInBackground(User... users) {
    Activity activity = activityRef.get();
    if (activity == null || activity.isFinishing()) {
      return false;
    }

    if (!getBankCertificate(activity.getApplicationContext())) {
      return false;
    }

    if (users.length == 0 || users[0] == null) {
      Log.w(TAG, "Null user, exiting");
      return null;
    }
    user = users[0]; // TODO: Authenticate this dude
    String username = user.getUsername();
    String password = user.getPassword();

    try (Channel channel = new Channel(bankHost, bankTcpPort)) {
      BaseEncoding base64 = BaseEncoding.base64();
      EllipticCurveDiffieHellman ecdh = new EllipticCurveDiffieHellman();

      String ownParam = base64.encode(ecdh.publicKey().getEncoded());

      channel.socket().setSoTimeout(5000);
      Message outbound = new Message(Message.KEY_EXCHANGE, ownParam);
      Log.d(TAG, logOutbound(channel.socket(), outbound));
      channel.write(outbound);

      while (!checkCancelled(channel) && !channel.isClosed()) {
        Message inbound = channel.read();
        Log.d(TAG, logInbound(channel.socket(), inbound));

        switch (inbound.opCode()) {
          case Message.KEY_EXCHANGE:
            byte[][] decoded = Arrays.stream(inbound.body().split(":"))
                .map(base64::decode)
                .toArray(byte[][]::new);
            byte[] sig = decoded[0];
            byte[] otherParam = decoded[1];

            PublicKey publicKey = bankCert.getPublicKey();
            String algo = "SHA256with" + publicKey.getAlgorithm();
            boolean validSignature = CryptoUtil.verifySignature(publicKey, otherParam, sig, algo);

            if (validSignature) {
              sessionKey = ecdh.generateSessionKey(otherParam);
              publishProgress("Got new session key");
              Log.v(TAG, "Key: " + base64.encode(sessionKey));
            } else {
              //TODO: handle this
              publishProgress("Invalid signature");
              return false;
            }

            byte[] pk = generateKeyPair(user.getUsername()).getEncoded();

            byte[] iv = new byte[16];
            new SecureRandom().nextBytes(iv);
            byte[] userData = (username + ":" + password).getBytes();
            byte[] hmac = Hashing.hmacSha256(sessionKey).newHasher()
                .putBytes(iv).putBytes(pk)
                .putString(username, StandardCharsets.UTF_8)
                .putString(password, StandardCharsets.UTF_8)
                .hash().asBytes();
            byte[] cipherText = CryptoUtil.encrypt(sessionKey, iv, userData);

            outbound.opCode(Message.AUTHENTICATE);
            outbound.body(Stream.of(hmac, iv, pk, cipherText)
                .map(base64::encode)
                .collect(Collectors.joining(":")));
            break;
          case Message.AUTHENTICATE:
            publishProgress("User authenticated");
            outbound.opCode(Message.DONE);
            outbound.body("Done here");
            Log.d(TAG, logOutbound(channel.socket(), outbound));
            channel.write(outbound);
            Log.d(TAG, logInbound(channel.socket(), channel.read()));

            KeyStore ks = KeyStore.getInstance("AndroidKeyStore");
            ks.load(null);

            SecretKey key = new SecretKeySpec(sessionKey, "AES");
            SecretKeyEntry entry = new KeyStore.SecretKeyEntry(key);
            ks.setEntry(user.getUsername() + "shared_key", entry,
                new KeyProtection.Builder(KeyProperties.PURPOSE_ENCRYPT)
                    .setBlockModes(KeyProperties.BLOCK_MODE_CTR)
                    .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_NONE).build());

            return true;
          case Message.USER_NOT_FOUND:
          case Message.MUFFIN_TIME:
            publishProgress(inbound.body());
            return false;
          default:
            Log.w(TAG, "Operation not implemented");
            Log.v(TAG, "opCode = " + inbound.opCode());
        }

        Log.d(TAG, logOutbound(channel.socket(), outbound));
        channel.write(outbound);
      }
    } catch (IOException ioe) {
      Log.e(TAG, "Socket issues: " + ioe.toString());
      Log.e(TAG, Log.getStackTraceString(ioe));
      publishProgress("Oops: " + ioe.toString());
    } catch (GeneralSecurityException gse) {
      Log.e(TAG, "Security problems: " + gse.toString());
      Log.e(TAG, Log.getStackTraceString(gse));
      publishProgress("Yikes: " + gse.getMessage());
    }

    return false;
  }

  private String logOutbound(Socket socket, Message msg) {
    return log(socket.getLocalSocketAddress(), socket.getRemoteSocketAddress(), msg);
  }

  private String logInbound(Socket socket, Message msg) {
    return log(socket.getRemoteSocketAddress(), socket.getLocalSocketAddress(), msg);
  }

  private String log(SocketAddress src, SocketAddress dest, Message msg) {
    return src + " ---> " + dest + " ::: " + msg;
  }

  private boolean checkCancelled(Channel channel) throws IOException {
    if (isCancelled()) {
      Log.d(TAG, "Task cancelled! Closing " + channel);
      channel.close();
      return true;
    }

    return false;
  }

  private boolean getBankCertificate(Context context) {
    CaService service = CaService.Companion.client(context).create(CaService.class);

    Response<String> response;
    try {
      response = service.getCertificate("bank").execute();
    } catch (IOException ioe) {
      Log.v(TAG, Log.getStackTraceString(ioe));
      return false;
    }

    if (response.isSuccessful() && response.body() != null) {
      try (InputStream is = context.getAssets().open("ca.pem");
          ByteArrayInputStream bais = new ByteArrayInputStream(response.body().getBytes())) {
        CertificateFactory cf = CertificateFactory.getInstance("X.509");
        bankCert = (X509Certificate) cf.generateCertificate(bais);
        Log.v(TAG, bankCert.getSubjectDN().getName());

        bankCert.checkValidity();
        publishProgress("Certificate doesn't appear to have expired");

        bankCert.verify(cf.generateCertificate(is).getPublicKey());
        publishProgress(TAG, "Verified bank's certificate");
        return true;
      } catch (Exception e) {
        Log.v(TAG, Log.getStackTraceString(e));
        return false;
      }
    }

    return false;
  }

  private PublicKey generateKeyPair(String username) {
    try {
      KeyPairGenerator kpg = KeyPairGenerator
          .getInstance(KeyProperties.KEY_ALGORITHM_EC, "AndroidKeyStore");
      KeyGenParameterSpec spec = new KeyGenParameterSpec.Builder(username + "private_key",
          KeyProperties.PURPOSE_SIGN | KeyProperties.PURPOSE_VERIFY)
          .setKeySize(256)
          .setDigests(KeyProperties.DIGEST_SHA256).build();

      kpg.initialize(spec);

      KeyPair kp = kpg.generateKeyPair();

      return kp.getPublic();
    } catch (NoSuchAlgorithmException | NoSuchProviderException | InvalidAlgorithmParameterException e) {
      e.printStackTrace();
      return null;
    }
  }
}
