package pt.ulisboa.tecnico.meic.sirs.a24.android.app.runnable;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.util.Arrays;
import java.util.stream.Stream;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;

import android.os.AsyncTask;
import android.util.Log;

import pt.ulisboa.tecnico.meic.sirs.a24.android.app.Transfer;
import pt.ulisboa.tecnico.meic.sirs.a24.android.app.User;
import pt.ulisboa.tecnico.meic.sirs.a24.security.CryptoUtil;

public class SendSmsTask extends AsyncTask<Transfer, Void, Void> {

  private static final String TAG = "SendSmsTask";

  private PrivateKey ownKey;
  private SecretKey sharedKey;
  private User user;

  private String host = System.getProperty("securesms.bank.host", "10.0.2.2");
  private Integer port = Integer.getInteger("securesms.bank.udp.port", 9696);

  public SendSmsTask(User user) {
    this.user = user;
  }

  @Override
  protected void onPreExecute() {
    super.onPreExecute();
    try {
      KeyStore ks = KeyStore.getInstance("AndroidKeyStore");
      ks.load(null);
      sharedKey = (SecretKey) ks.getKey(user.getUsername() + "shared_key", null);
      ownKey = (PrivateKey) ks.getKey(user.getUsername() + "private_key", null);

      /*try(FileInputStream fis = new FileInputStream(System.getProperty("securesms.keystore
      .name"))) {
        ks.load(fis, System.getProperty("securesms.keystore.name").toCharArray());
        ks.getEntry(user.getUsername() + "shared_key")
      }*/
    } catch (Exception pokemon) {
      pokemon.printStackTrace();
    }
  }

  @Override
  protected Void doInBackground(Transfer... transfers) {
    Charset charset = StandardCharsets.UTF_8;

    Transfer transfer = transfers[0];
    String iban = transfer.getIban();
    String amount = transfer.getAmount();

    try (DatagramSocket socket = new DatagramSocket()) {
      Cipher cipher = Cipher.getInstance("AES/CTR/NoPadding");
      cipher.init(Cipher.ENCRYPT_MODE, sharedKey);
      byte[] iv = cipher.getIV();
      byte[] encrypted = cipher.doFinal((iban + " " + amount).getBytes(charset));

      byte[][] trans = Stream.of(iban, amount)
          .map(s -> s.getBytes(charset))
          .toArray(byte[][]::new);
      byte[] toSign = ByteBuffer.allocate(bytesLength(trans[0], trans[1], iv))
          .put(trans[0]).put(trans[1]).put(iv)
          .compact().array();

      byte[] signature = CryptoUtil.sign(ownKey, toSign, "SHA256withECDSA");

      byte[] username = user.getUsername().getBytes(charset);

      byte[] bytes = ByteBuffer.allocate(bytesLength(iv, username, encrypted, signature) + 3)
          .put((byte) username.length).put((byte) encrypted.length).put((byte) signature.length)
          .put(username).put(iv).put(encrypted).put(signature)
          .array();

      Log.d(TAG, Arrays.toString(bytes));
      socket.connect(new InetSocketAddress(host, port));
      DatagramPacket packet = new DatagramPacket(bytes, bytes.length, socket.getRemoteSocketAddress());
      socket.send(packet);

      //TODO: decide what to do next
    } catch (Exception e) {
      Log.e(TAG, Log.getStackTraceString(e));
    }

    return null;
  }

  private int bytesLength(byte[]... bytes) {
    return Stream.of(bytes)
        .mapToInt(b -> b.length)
        .sum();
  }

}
