Required Software

Java 1.8.0
Java 11.0.1
Android SDK
Android Virtual Device (AVD)



Setup:

- Set JAVA_HOME to Java 11 install path and ANDROID_HOME to Android SDK install path
- Create AVD with API level 26 or above //TODO: add command to create this
- In the Android SDK's tools/bin directory run the following command: avdmanager create avd -k "system-images;android-28;google_apis;x86" -d pixel -n <avd name>
- Change to the /emulator directory of the Android SDK and run emulator -avd <avd name>


Running the project:

- In the project's root directory run (in separate terminals):
	gradlew :app:installDebug
	gradlew :bank:run
	gradlew :ca:run


	
From this point onwards, there are 3 (user, password) pairs registered in the bank's server, namely:
(Amy, 100101),
(Jimmy, 777333),
(Timmy, 654321).


The expected messages have the following structure

"<target_iban> <amount in cents>"

the target IBAN's max size is 34 characters
the max size of the amount is 6 characters (999999 corresponds to 9999,99€)


Upon a successful transfer request, the server will print the user, amount and destination IBAN.
