package pt.ulisboa.tecnico.meic.sirs.a24.hibernate;

import com.google.common.base.CaseFormat;

import org.hibernate.boot.model.naming.Identifier;
import org.hibernate.boot.model.naming.PhysicalNamingStrategy;
import org.hibernate.engine.jdbc.env.spi.JdbcEnvironment;

public class SnakeCasePhysicalNamingStrategy implements PhysicalNamingStrategy {

  @Override
  public Identifier toPhysicalCatalogName(Identifier name, JdbcEnvironment jdbcEnvironment) {
    // No need to translate
    return name;
  }

  @Override
  public Identifier toPhysicalSchemaName(Identifier name, JdbcEnvironment jdbcEnvironment) {
    // No need to translate
    return name;
  }

  @Override
  public Identifier toPhysicalTableName(Identifier name, JdbcEnvironment jdbcEnvironment) {
    String snakeCase = fromCamelToSnakeCase(name.getText());
    return jdbcEnvironment.getIdentifierHelper().toIdentifier(snakeCase, name.isQuoted());
  }

  @Override
  public Identifier toPhysicalSequenceName(Identifier name, JdbcEnvironment jdbcEnvironment) {
    String snakeCase = fromCamelToSnakeCase(name.getText());

    if (!snakeCase.endsWith("_seq")) {
      snakeCase += "_seq";
    }

    return jdbcEnvironment.getIdentifierHelper().toIdentifier(snakeCase, name.isQuoted());
  }

  @Override
  public Identifier toPhysicalColumnName(Identifier name, JdbcEnvironment jdbcEnvironment) {
    String snakeCase = fromCamelToSnakeCase(name.getText());
    return jdbcEnvironment.getIdentifierHelper().toIdentifier(snakeCase, name.isQuoted());
  }

  private String fromCamelToSnakeCase(String name) {
    return CaseFormat.LOWER_CAMEL
        .to(CaseFormat.LOWER_UNDERSCORE, name)
        .replaceAll("_+", "_");
  }
}