package pt.ulisboa.tecnico.meic.sirs.a24.security;

import java.nio.ByteBuffer;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.ECGenParameterSpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.crypto.KeyAgreement;

import com.google.common.hash.Hashing;

public class EllipticCurveDiffieHellman {

  private KeyPair kp;
  private KeyPairGenerator kpg;
  private boolean used = false;

  public EllipticCurveDiffieHellman() {
    try {
      kpg = KeyPairGenerator.getInstance("EC");
      kpg.initialize(new ECGenParameterSpec("secp256r1"));
    } catch (NoSuchAlgorithmException | InvalidAlgorithmParameterException se) {
      throw new RuntimeException(se);
    }

    kp = kpg.generateKeyPair();
  }

  public PublicKey publicKey() {
    return kp.getPublic();
  }

  public byte[] generateSessionKey(byte[] otherPk) {
    byte[] key = new byte[16]; // 128-bits
    byte[] ownPk = kp.getPublic().getEncoded();

    List<ByteBuffer> params = Stream.of(ownPk, otherPk)
        .map(ByteBuffer::wrap)
        .sorted()
        .collect(Collectors.toList());

    Hashing.hmacSha256(generateSharedSecret(otherPk)).newHasher()
        .putBytes(params.get(0))
        .putBytes(params.get(1))
        .hash().writeBytesTo(key, 0, key.length);

    return key;
  }

  private byte[] generateSharedSecret(byte[] otherPk) {
    if (used) {
      kp = kpg.generateKeyPair();
      used = false;
    }

    try {
      KeyFactory kf = KeyFactory.getInstance("EC");
      KeySpec pkSpec = new X509EncodedKeySpec(otherPk);
      PublicKey otherPublicKey = kf.generatePublic(pkSpec);

      KeyAgreement ka = KeyAgreement.getInstance("ECDH");
      ka.init(kp.getPrivate());
      ka.doPhase(otherPublicKey, true);

      used = true;
      return ka.generateSecret();
    } catch (NoSuchAlgorithmException | InvalidKeySpecException | InvalidKeyException se) {
      throw new RuntimeException(se);
    }
  }


}
