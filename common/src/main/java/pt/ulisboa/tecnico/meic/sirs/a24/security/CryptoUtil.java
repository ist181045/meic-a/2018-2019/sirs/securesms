package pt.ulisboa.tecnico.meic.sirs.a24.security;

import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.util.Base64;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import com.google.common.hash.Hashing;

public class CryptoUtil {

  public static byte[] encrypt(byte[] key, byte[] iv, byte[] input) {
    return encrypt(new SecretKeySpec(key, "AES"), new IvParameterSpec(iv), input);
  }

  public static byte[] decrypt(byte[] key, byte[] iv, byte[] input) {
    return decrypt(new SecretKeySpec(key, "AES"), new IvParameterSpec(iv), input);
  }

  public static byte[] encrypt(SecretKey key, byte[] iv, byte[] input) {
    return encrypt(key, new IvParameterSpec(iv), input);
  }

  public static byte[] decrypt(SecretKey key, byte[] iv, byte[] input) {
    return decrypt(key, new IvParameterSpec(iv), input);
  }

  public static byte[] encrypt(byte[] key, IvParameterSpec iv, byte[] input) {
    return encrypt(new SecretKeySpec(key, "AES"), iv, input);
  }

  public static byte[] decrypt(byte[] key, IvParameterSpec iv, byte[] input) {
    return decrypt(new SecretKeySpec(key, "AES"), iv, input);
  }

  public static byte[] encrypt(SecretKey key, IvParameterSpec iv, byte[] input) {
    return symCipher(Cipher.ENCRYPT_MODE, key, iv, input);
  }

  public static byte[] decrypt(SecretKey key, IvParameterSpec iv, byte[] input) {
    return symCipher(Cipher.DECRYPT_MODE, key, iv, input);
  }

  public static byte[] sign(PrivateKey key, byte[] message, String algo)
      throws InvalidKeyException {
    byte[] hashed = Hashing.sha256().hashBytes(message).asBytes();

    try {
      Signature sig = Signature.getInstance(algo);
      sig.initSign(key);
      sig.update(hashed);

      return sig.sign();
    } catch (NoSuchAlgorithmException | SignatureException ignored) {
      throw new SecurityException(ignored);
    }
  }

  public static boolean verifySignature(PublicKey key, byte[] message, byte[] signature,
      String algo) throws InvalidKeyException {
    byte[] hashed = Hashing.sha256().hashBytes(message).asBytes();

    try {
      Signature sig = Signature.getInstance(algo);
      sig.initVerify(key);
      sig.update(hashed);

      return sig.verify(signature);
    } catch (NoSuchAlgorithmException | SignatureException ignored) {
      throw new SecurityException(ignored);
    }
  }

  private static byte[] symCipher(int opmode, SecretKey key, IvParameterSpec ivSpec, byte[] input) {
    try {
      Cipher cipher = Cipher.getInstance("AES/CTR/NoPadding");
      cipher.init(opmode, key, ivSpec);

      return cipher.doFinal(input);
    } catch (InvalidAlgorithmParameterException | InvalidKeyException e) {
      e.printStackTrace(); // TODO: Be handled
      return new byte[]{-1};
    } catch (GeneralSecurityException ignored) {
      throw new SecurityException(ignored);
    }
  }
}
