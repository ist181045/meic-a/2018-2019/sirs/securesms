package pt.ulisboa.tecnico.meic.sirs.a24;

import java.util.Objects;

import pt.ulisboa.tecnico.meic.sirs.a24.util.Pair;

public class Message {

  private Pair<Integer, String> message;

  public static final int USER_NOT_FOUND = -2;

  public static final int MUFFIN_TIME = -1;

  public static final int KEY_EXCHANGE = 1;
  public static final int AUTHENTICATE = 2;
  public static final int DONE = 3;

  public Message(int opCode, String text) {
    message = new Pair<>(opCode, text);
  }

  public int opCode() {
    return message.first();
  }

  public void opCode(int opCode) {
    message.first(opCode);
  }

  public String body() {
    return message.second();
  }

  public void body(String body) {
    message.second(body);
  }

  @Override
  public int hashCode() {
    return Objects.hash(message);
  }

  @Override
  public boolean equals(Object o) {
    if (o instanceof Message) {
      Message other = (Message) o;
      return Objects.equals(message, other.message);
    }

    return false;
  }

  @Override
  public String toString() {
    return "{ opCode: " + opCode() + "; body: " + body() + " }";
  }
}
