package pt.ulisboa.tecnico.meic.sirs.a24.util;

import java.util.Objects;

public class Pair<F, S> {
  private F first;
  private S second;

  public Pair(F first, S second) {
    this.first = first;
    this.second = second;
  }

  public F first() {
    return first;
  }

  public void first(F first) {
    this.first = first;
  }

  public S second() {
    return second;
  }

  public void second(S second) {
    this.second = second;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj instanceof Pair) {
      Pair pair = (Pair) obj;
      return Objects.equals(first, pair.first()) && Objects.equals(second, pair.second());
    }

    return false;
  }

  @Override
  public int hashCode() {
    return Objects.hash(first, second);
  }

  @Override
  public String toString() {
    return "(" + first + ", " + second + ")";
  }
}
