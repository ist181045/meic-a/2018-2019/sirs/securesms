package pt.ulisboa.tecnico.meic.sirs.a24;

import java.io.Closeable;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class Channel implements Closeable {

  private Socket socket;

  public Channel(String host, int port) throws IOException {
    this(new Socket(host, port));
  }

  public Channel(Socket socket) {
    this.socket = socket;
  }

  public Socket socket() {
    return socket;
  }

  public boolean isClosed() {
    return socket.isClosed();
  }

  public Message read() throws IOException {
    DataInputStream dis = new DataInputStream(socket.getInputStream());
    return new Message(dis.readInt(), dis.readUTF());
  }

  public void write(Message message) throws IOException {
    write(message.opCode(), message.body());
  }

  public void write(int opCode, String message) throws IOException {
    DataOutputStream dos = new DataOutputStream(socket.getOutputStream());
    dos.writeInt(opCode);
    dos.writeUTF(message);
    dos.flush();
  }

  @Override
  public void close() throws IOException {
    if (!socket.isClosed()) {
      socket.close();
    }
  }

  @Override
  public String toString() {
    return "Channel{" + socket + "}";
  }
}
